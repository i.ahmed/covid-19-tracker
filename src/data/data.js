export const casesTypeColors = {
    cases: {
        color: '#ffbf1a',
        multi: 800
    },
    deaths: {
        color: '#f25076',
        multi: 800
    },
    recovered: {
        color: '#40c0b7',
        multi: 800
    }
};

export const links = [
    {
        label: 'Home',
        to: '/'
    },
    {
        label: 'MAP',
        to: '/map'
    },
    {
        label: 'TABLE',
        to: '/table'
    },
    {
        label: 'CHARTS',
        to: '/charts'
    }
];