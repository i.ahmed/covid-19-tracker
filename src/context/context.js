import React,{useContext, useState} from 'react';
import {
    fetchAllCovidData, 
    fetchDataForCountries, 
    fetchContinentsChartData, 
    fetchCountriesChartData} from '../utils/api';
const AppContext = React.createContext();
const AppProvider = ({children}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isDark, setIsDark] = useState(false);
    return <AppContext.Provider value={{
        isLoading,
        isDark,
        setIsDark,
        setIsLoading,
        fetchAllCovidData, 
        fetchDataForCountries,
        fetchCountriesChartData,
        fetchContinentsChartData
    }}>
        {children}
    </AppContext.Provider>
}

const useGlobalContext = () => {
    return useContext(AppContext);
}

export { useGlobalContext, AppProvider};