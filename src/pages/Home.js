import React from "react";
import CardList from "../componants/CardList";
import Header from "../componants/Header";
import ThemeSwitch from "../componants/ThemeSwitch";

const Home = () => {
  return (
    <div className="container">
      <ThemeSwitch/>
      <Header page="home" />
      <CardList />
    </div>
  );
};

export default Home;
