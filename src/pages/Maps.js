import React, { useState, useEffect } from "react";
import Filters from "../componants/Filters";
import Header from "../componants/Header";
import Map from "../componants/Map";
import { casesTypeColors } from "../data/data";
import { useGlobalContext } from "../context/context";
import Loading from "../componants/Loading";
import ThemeSwitch from "../componants/ThemeSwitch";

const Maps = () => {
  const [caseType, setCaseType] = useState("cases");
  const [country, setCountry] = useState("all");
  const [countries, setCountries] = useState([]);
  const [center, setCenter] = useState([34.80746, -40.4796]);
  const { fetchDataForCountries, isLoading, setIsLoading } = useGlobalContext();
  useEffect(() => {
    fetchDataForCountries(setCountries, setIsLoading);
  }, []);
  useEffect(() => {
    fetchDataForCountries(setCountries, setIsLoading, country, setCenter);
  }, [country]);
  return (
    <div className="container">
      <ThemeSwitch/>  
      <Header page="map" />
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <Filters
            countryName={country}
            setCaseType={setCaseType}
            setCountry={setCountry}
            countries={countries}
            casesTypeColors={casesTypeColors}
          />
          <Map
            center={center}
            caseType={caseType}
            countries={countries}
            casesTypeColors={casesTypeColors}
          />
        </>
      )}
    </div>
  );
};

export default Maps;
