import React from "react";
import ChartList from "../componants/ChartList";
import Header from "../componants/Header";
import ThemeSwitch from "../componants/ThemeSwitch";

const Charts = () => {
  return (
    <div className="container">
      <ThemeSwitch />
      <Header page="charts" />
      <ChartList />
    </div>
  );
};

export default Charts;
