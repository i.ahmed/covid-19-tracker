import React, { useState, useEffect } from "react";
import Header from "../componants/Header";
import Loading from "../componants/Loading";
import Pagination from "../componants/Pagination";
import ThemeSwitch from "../componants/ThemeSwitch";
import { useGlobalContext } from "../context/context";
const Table = () => {
  const [search, setSearch] = useState("");
  const [rows, setRows] = useState(10);
  const [start, setStart] = useState(0);
  const [countries, setCountries] = useState([]);
  const { fetchDataForCountries, isLoading, setIsLoading } = useGlobalContext();
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (name === "search") {
      setSearch(value);
    } else {
      setRows(parseInt(value.trim()));
    }
  };
  const handleSearch = (e) => {
    if (e.key === "Enter") {
      setSearch(e.target.value.trim());
    }
  };
  useEffect(() => {
    fetchDataForCountries(setCountries, setIsLoading);
  }, []);
  useEffect(() => {
    fetchDataForCountries(setCountries, setIsLoading, search);
  }, [search]);
  return (
    <section className="container">
      <ThemeSwitch/>
      <Header page="table" />
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <div className="search-wrapper">
            <input
              type="text"
              name="search"
              placeholder="Enter country name and press enter"
              onKeyPress={(e) => handleSearch(e)}
            />
            <article className="pagination">
              <Pagination
                length={countries.length}
                start={start}
                setStart={setStart}
                rows={rows}
              />
            </article>
            <div>
              <span>rows : </span>
              <select
                name="rows"
                value={rows}
                onChange={(e) => handleChange(e)}
              >
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
              </select>
            </div>
          </div>
          <article className="table-container">
            <table border={1}>
              <thead>
                <tr>
                  <th>flag</th>
                  <th>continent</th>
                  <th>name</th>
                  <th className="normal">population</th>
                  <th className="cases">cases</th>
                  <th className="deaths">deaths</th>
                  <th className="recoverd">recovered</th>
                </tr>
              </thead>
              <tbody>
                {countries.map((country, index) => {
                  if (index >= start && index < start + (rows - 1)) {
                    return (
                      <tr key={index}>
                        <td>
                          <img
                            src={country.countryInfo.flag}
                            alt={country.country}
                          />
                        </td>
                        <td>{country.continent}</td>
                        <td>{country.country}</td>
                        <td className="normal">{country.population}</td>
                        <td className="cases">{country.cases}</td>
                        <td className="deaths">{country.deaths}</td>
                        <td className="recoverd">{country.recovered}</td>
                      </tr>
                    );
                  }
                })}
              </tbody>
            </table>
          </article>
        </>
      )}
    </section>
  );
};

export default Table;
