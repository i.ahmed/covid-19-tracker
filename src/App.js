import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Charts from "./pages/Charts";
import Home from "./pages/Home";
import Maps from "./pages/Maps";
import Table from "./pages/Table";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/map" component={Maps} />
        <Route path="/table" component={Table} />
        <Route path="/charts" component={Charts} />
      </Switch>
    </Router>
  );
}

export default App;
