import fetchData from "../utils/request";
const BASE_URL = "https://disease.sh/v3/covid-19";

export const fetchAllCovidData = async (setData, setIsLoading) => {
  setIsLoading(true);
  const data = await fetchData(`${BASE_URL}/all`);
  setData(data);
  setIsLoading(false);
};

export const fetchDataForCountries = async (
  setCountries,
  setIsLoading,
  country = "",
  setCenter = null
) => {
  setIsLoading(true);
  let data = await fetchData(
    `${BASE_URL}/countries${
      country !== "" && country !== "all" ? `/${country}` : ""
    }`
  );
  if (!Array.isArray(data)) {
    if (setCenter !== null) {
      setCenter([data.countryInfo.lat, data.countryInfo.long]);
    }
    data = !Object.keys(data).includes("message") ? [data] : [];
  }
  setCountries(data);
  setIsLoading(false);
};

export const fetchCountriesChartData = async (
  setCountriesData,
  setCountriesLables,
  setIsLoading
) => {
  setIsLoading(true);
  const data = await fetchData(`${BASE_URL}/countries`);
  const labels = [];
  const cases = [];
  data.forEach((item) => {
    labels.push(item.country);
    cases.push(item.cases);
  });
  setCountriesLables(labels);
  setCountriesData(cases);
  setIsLoading(false);
};

export const fetchContinentsChartData = async (
  setContinentsCases,
  setContinentsDeaths,
  setContinentsRecovers,
  setContinentLables,
  setContinentsCritical,
  setIsLoading
) => {
  setIsLoading(true);
  const data = await fetchData(`${BASE_URL}/continents`);
  const labels = [];
  const cases = [];
  const deaths = [];
  const recovers = [];
  const criticals = [];
  data.forEach((item) => {
    labels.push(item.continent);
    cases.push(item.cases);
    deaths.push(item.deaths);
    recovers.push(item.recovered);
    criticals.push(item.critical);
  });
  setContinentLables(labels);
  setContinentsCases(cases);
  setContinentsDeaths(deaths);
  setContinentsRecovers(recovers);
  setContinentsCritical(criticals);
  setIsLoading(false);
};
