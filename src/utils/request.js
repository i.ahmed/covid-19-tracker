const fetchData = async (url) => {
  const response = await fetch(`${url}`);
  const data = response.state !== 404 ? await response.json() : [];
  return data;
};

export default fetchData;
