import React from "react";
import logo from "../images/covid-19.svg";

const Loading = () => {
  return (
    <section className="loading-container">
      <img src={logo} alt="loading" className="rotate" />
    </section>
  );
};

export default Loading;
