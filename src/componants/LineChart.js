import React from "react";
import { Line } from "react-chartjs-2";
const LineChart = ({
  axis,
  title,
  label,
  backgroundColor,
  borderColor,
  chartData,
  labels,
}) => {
  const data = {
    labels: labels,
    datasets: [
      {
        label,
        data: chartData,
        backgroundColor,
        borderColor,
        fill: true
      },
    ],
  };

  const options = {
    scales: {
        yAxes: [
        {
            ticks: {
            beginAtZero: true,
            },
        },
        ],
    },
    plugins: {
      title: {
        display: true,
        text: title,
      },
    }
  };
  return (
    <article
      className="card"
      style={{
        maxHeight: `none`,
        flex: `100%`
      }}
    >
      <Line data={data} options={options} />
    </article>
  );
};

export default LineChart;
