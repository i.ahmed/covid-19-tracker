import React from 'react';
import Toggle from "react-toggle";
import {useGlobalContext} from '../context/context';
import "react-toggle/style.css";
const ThemeSwitch = () => {
    const {isDark, setIsDark} = useGlobalContext(); 

    React.useEffect(() => {
      document.querySelector('body').classList = `${isDark && 'dark'}`;
      return () => {

      }
    }, [isDark])
    return (
      <label className="theme-switch-wrapper">
        <Toggle
          checked={isDark}
          icons={{
            unchecked: <span>🌞</span> ,
            checked: <span>🌜</span>,
          }}
          onChange={() => setIsDark(!isDark)}/>
      </label>
    );
}

export default ThemeSwitch
