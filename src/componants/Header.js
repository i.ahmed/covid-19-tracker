import React from "react";
import { Link } from "react-router-dom";
import { links } from "../data/data";

const Header = ({ page }) => {
  return (
    <nav className="header">
      <h1>
        COVID-<span>19</span>
      </h1>
      <ul>
        {links.map((link, index) => {
          return (
            <li
              key={index}
              className={`${page === link.label.toLowerCase() && "active"}`}
            >
              <Link to={link.to}>{link.label}</Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default Header;
