import React from "react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";

const Pagination = ({ length, start, setStart, rows }) => {
  const handleClick = (e) => {
    if (e.target.id === "increse") {
      const validStart = start + rows - 1;
      if (validStart < length) {
        setStart((value) => value + rows - 1);
      }
    } else {
      if (start > 0) {
        setStart((value) => {
          return value - (rows - 1);
        });
      }
    }
  };
  return (
    <article>
      <span>
        <AiOutlineArrowLeft id="decrese" onClick={(e) => handleClick(e)} />
      </span>
      <span>
        <AiOutlineArrowRight id="increse" onClick={(e) => handleClick(e)} />
      </span>
    </article>
  );
};

export default Pagination;
