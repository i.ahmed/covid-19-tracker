import React from "react";
import { Bar } from "react-chartjs-2";
const HorizontalBarChart = ({
  axis,
  title,
  label,
  backgroundColor,
  borderColor,
  chartData,
  labels,
}) => {
  const data = {
    labels: labels,
    datasets: [
      {
        label,
        data: chartData,
        backgroundColor,
        borderColor,
        borderWidth: 1
      },
    ],
  };

  const options = {
    indexAxis: axis,
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
    plugins: {
      title: {
        display: true,
        text: title,
      },
    },
  };
  return (
    <article
      className="card"
      style={{
        maxHeight: `none`,
        flex: `33%`
      }}
    >
      <Bar data={data} options={options} />
    </article>
  );
};

export default HorizontalBarChart;
