import React from "react";

const Filters = ({
  countries,
  countryName,
  casesTypeColors,
  setCaseType,
  setCountry,
}) => {
  const handleChange = (e) => {
    if (e.target.name === "country-select") {
      setCountry(e.target.value.trim());
    } else {
      setCaseType(e.target.value.trim());
    }
  };
  return (
    <section className="filter-container">
      <select name="country-select" onChange={handleChange} value={countryName}>
        <option value="all">All Countries</option>
        {countries.map((country, index) => {
          return (
            <option key={index} value={country.country}>
              {country.country.toUpperCase()}
            </option>
          );
        })}
      </select>
      <select name="case-select" onChange={handleChange}>
        {Object.keys(casesTypeColors).map((type, index) => {
          return (
            <option key={index} value={type}>
              {type.toUpperCase()}
            </option>
          );
        })}
      </select>
    </section>
  );
};

export default Filters;
