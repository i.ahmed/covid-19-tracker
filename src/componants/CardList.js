import React, { useState, useEffect } from "react";
import Card from "./Card";
import Loading from "./Loading";
import { useGlobalContext } from "../context/context";
import DeathSvg from "../images/covid-19-red.svg";
import RecoveredSvg from "../images/covid-19-green.svg";
import ActivelSvg from "../images/covid-19.svg";
import NormalSvg from "../images/covid-19-yellow.svg";
const CardList = () => {
  const [data, setData] = useState({});
  const { isLoading, setIsLoading, fetchAllCovidData } = useGlobalContext();
  useEffect(() => {
    fetchAllCovidData(setData, setIsLoading);
  }, []);
  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <section className="card-container">
          <Card title={"Cases"} image={NormalSvg} count={data.cases} />
          <Card title={"Active"} image={ActivelSvg} count={data.active} />
          <Card title={"Critical"} image={DeathSvg} count={data.critical} />
          <Card title={"Deaths"} image={DeathSvg} count={data.deaths} />
          <Card
            title={"Recovered"}
            image={RecoveredSvg}
            fill={true}
            count={data.recovered}
          />
          <Card
            title={"Cases (Today)"}
            image={NormalSvg}
            count={data.todayCases}
          />
          <Card
            title={"Deaths (Today)"}
            image={DeathSvg}
            count={data.todayDeaths}
          />
          <Card
            title={"Recovered (Today)"}
            image={RecoveredSvg}
            count={data.todayRecovered}
          />
        </section>
      )}
    </>
  );
};

export default CardList;
