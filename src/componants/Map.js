import React from "react";
import { MapContainer, TileLayer, Circle, Popup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
const Map = ({ caseType, countries, center, casesTypeColors }) => {
  return (
    <section className="map-container">
      <MapContainer center={center} zoom={3} scrollWheelZoom={false}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {countries.map((country) => {
          return (
            <Circle
              key={country.country}
              pathOptions={{
                color: casesTypeColors[caseType].color,
                fillColor: casesTypeColors[caseType].color,
              }}
              fillOpacity={0.4}
              radius={
                Math.sqrt(country[caseType]) * casesTypeColors[caseType].multi
              }
              center={[country.countryInfo.lat, country.countryInfo.long]}
            >
              <Popup>{country.country}</Popup>
            </Circle>
          );
        })}
      </MapContainer>
    </section>
  );
};

export default Map;
