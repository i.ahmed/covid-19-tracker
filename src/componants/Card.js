import React from "react";

const Card = ({ title, count, image, fill = false }) => {
  return (
    <article className="card" style={{ flex: fill && "41%" }}>
      <h3>{title}</h3>
      <p>{count}</p>
      <div className="image-container">
        <img src={image} alt="covid logo" />
      </div>
    </article>
  );
};

export default Card;
