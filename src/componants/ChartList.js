import React, { useState, useEffect } from "react";
import HorizontalBarChart from "./HorizontalBarChart";
import { casesTypeColors } from "../data/data";
import { useGlobalContext } from "../context/context";
import Loading from "./Loading";
import LineChart from "./LineChart";

const ChartList = () => {
  const [continentsCases, setContinentsCases] = useState([]);
  const [continentsDeaths, setContinentsDeaths] = useState([]);
  const [continentsRecovers, setContinentsRecovers] = useState([]);
  const [continentsCritical, setContinentsCritical] = useState([]);
  const [continentLables, setContinentLables] = useState([
    "Red",
    "Blue",
    "Yellow",
    "Green",
    "Purple",
    "Orange",
  ]);
  const [countriesData, setCountriesData] = useState([12, 19, 3, 5, 2, 3]);
  const [countriesLables, setCountriesLables] = useState([
    "Red",
    "Blue",
    "Yellow",
    "Green",
    "Purple",
    "Orange",
  ]);
  const {
    isLoading,
    setIsLoading,
    fetchCountriesChartData,
    fetchContinentsChartData,
  } = useGlobalContext();

  useEffect(() => {
    fetchContinentsChartData(
      setContinentsCases,
      setContinentsDeaths,
      setContinentsRecovers,
      setContinentLables,
      setContinentsCritical,
      setIsLoading
    );
    fetchCountriesChartData(setCountriesData, setCountriesLables, setIsLoading);
  }, []);
  return (
    <React.Fragment>
      {isLoading ? (
        <Loading />
      ) : (
        <section className="card-container">
          <HorizontalBarChart
            axis="x"
            label="Cases"
            chartData={continentsCases}
            labels={continentLables}
            backgroundColor={casesTypeColors.cases.color}
            borderColor={casesTypeColors.cases.color}
            title="Continents"
          />
          <HorizontalBarChart
            axis="x"
            label="Deaths"
            chartData={continentsDeaths}
            labels={continentLables}
            backgroundColor={casesTypeColors.deaths.color}
            borderColor={casesTypeColors.deaths.color}
            title="Continents"
          />
          <HorizontalBarChart
            axis="x"
            label="Criticals"
            chartData={continentsCritical}
            labels={continentLables}
            backgroundColor={casesTypeColors.deaths.color}
            borderColor={casesTypeColors.deaths.color}
            title="Continents"
          />
          <HorizontalBarChart
            axis="x"
            label="Recovers"
            chartData={continentsRecovers}
            labels={continentLables}
            backgroundColor={casesTypeColors.recovered.color}
            borderColor={casesTypeColors.recovered.color}
            title="Continents"
          />
          <LineChart
            axis="x"
            label="Cases"
            chartData={countriesData}
            labels={countriesLables}
            backgroundColor={casesTypeColors.cases.color}
            title="Countries Covid Data"
          />
        </section>
      )}
    </React.Fragment>
  );
};

export default ChartList;
